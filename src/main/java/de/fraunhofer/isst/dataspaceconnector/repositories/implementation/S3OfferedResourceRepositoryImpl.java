package de.fraunhofer.isst.dataspaceconnector.repositories.implementation;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.fraunhofer.isst.dataspaceconnector.config.AwsConfiguration;
import de.fraunhofer.isst.dataspaceconnector.model.OfferedResource;
import de.fraunhofer.isst.dataspaceconnector.model.ResourceMetadata;
import de.fraunhofer.isst.dataspaceconnector.repositories.OfferedResourceRepository;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Specific Amazon S3 OfferedResourceRepository implementation.
 * Provides the means to connect to AWS via the {@link AwsConfiguration} configuration
 */
@Repository
@ConditionalOnBean(AwsConfiguration.class)
@Configurable
public class S3OfferedResourceRepositoryImpl implements OfferedResourceRepository {
  private static final Logger LOGGER = LoggerFactory.getLogger(S3OfferedResourceRepositoryImpl.class);
  /**
   * Amazon S3 client used for the communication with S3
   */
  private final AmazonS3 s3client;
  /**
   * A static bucket name holding the resources
   */
  private final String bucket;
  /**
   * An ObjectMapper to (de)serialize Resource metadata to/from JSON
   */
  private final ObjectMapper mapper = new ObjectMapper();

  /**
   * Constructor for S3OfferedResourceRepositoryImpl. Sets up the connection with S3.
   *
   * @param awsConfiguration The AWS configuration {@link AwsConfiguration}
   */
  @Autowired
  public S3OfferedResourceRepositoryImpl(AwsConfiguration awsConfiguration) {
    AWSCredentials credentials = new BasicAWSCredentials(awsConfiguration.getAccessKey(), awsConfiguration.getSecretKey());
    bucket = awsConfiguration.getBucket();
    s3client = AmazonS3ClientBuilder
        .standard()

        .withCredentials(new AWSStaticCredentialsProvider(credentials))
        .withRegion(Regions.fromName(awsConfiguration.getRegion()))
        .build();

    LOGGER.info("Created S3 Client");
  }

  /**
   * Deletes a specific resource (both data and metadata) from the S3 bucket
   *
   * @param uuid The resource uuid
   */
  @Override
  public void deleteById(@NotNull UUID uuid) {
    try {
      s3client.deleteObjects(new DeleteObjectsRequest(bucket).withKeys(uuid + "/data", uuid + "/metadata.json"));
    } catch (SdkClientException e) {
      LOGGER.warn("Error in deleting resource on S3 storage: {}", e.getMessage());
    }
  }

  /**
   * Find a specific resource and retrieve the data and metadata
   *
   * @param uuid The resource uuid
   * @return An optional {@link OfferedResource} when found
   */
  @NotNull
  @Override
  public Optional<OfferedResource> findById(UUID uuid) {
    return s3ObjectSummariesToResource(s3client.listObjects(bucket, uuid.toString()).getObjectSummaries(), true).stream().findFirst();
  }

  /**
   * Find all resources in the S3 bucket, only metadata
   *
   * @return A list of {@link OfferedResource}
   */
  @NotNull
  @Override
  public List<OfferedResource> findAll() {
    return s3ObjectSummariesToResource(s3client.listObjects(bucket).getObjectSummaries(), false);
  }

  /**
   * Store a Resource in the S3 bucket.
   * The model used for storing resources is:
   * /{ResourceID}/data
   * /{ResourceID}/metadata.json
   * 
   * @param resource The resource to be stored
   * @param <S> The offered resource type
   * @return The resource that has been stored or null in case of exceptions
   */
  @Override
  public <S extends OfferedResource> S save(S resource) {
    try {
      if (resource.getData() != null) {
        s3client.putObject(bucket, resource.getUuid() + "/data", resource.getData());
      }
      s3client.putObject(bucket, resource.getUuid() + "/metadata.json", mapper.writeValueAsString(resource.getResourceMetadata()));
      return resource;
    } catch (JsonProcessingException e) {
      LOGGER.warn("Error in persisting resource metadata on S3 storage: {}", e.getMessage());
    } catch (SdkClientException e) {
      LOGGER.warn("Error in persisting resource on S3 storage: {}", e.getMessage());
    }
    return null;
  }

  /**
   * Helper function to transform S3 file structure to a list of {@link OfferedResource}
   * The model used for retrieving resources is:
   * /{ResourceID}/data
   * /{ResourceID}/metadata.json
   * 
   * @param objectSummaries The list of {@link S3ObjectSummary}s that will be converted to the {@link OfferedResource}s
   * @param includeData Indicate whether or not the data itself should be part of the transformation
   * @return The list of {@link OfferedResource}s that are successfully parsed
   */
  private List<OfferedResource> s3ObjectSummariesToResource(List<S3ObjectSummary> objectSummaries, boolean includeData) {
    return objectSummaries.stream()
        // Group S3 objects by ResourceID
        .collect(Collectors.groupingBy(object -> object.getKey().substring(0, object.getKey().indexOf('/'))))
        .entrySet().parallelStream().map(entry -> {
          try {
            // Retrieve metadata.json and parse to ResourceMetadata
            Optional<S3ObjectSummary> metadataObject = entry.getValue().stream().filter(object -> object.getKey().endsWith("/metadata.json")).findFirst();
            if (metadataObject.isEmpty()) {
              LOGGER.warn("Metadata of resource {} cannot be found", entry.getKey());
              return null;
            }
            InputStream metadataIs = s3client.getObject(bucket, metadataObject.get().getKey()).getObjectContent();
            ResourceMetadata resourceMetadata = mapper.readValue(metadataIs, ResourceMetadata.class);

            // Retrieve data if the includeData parameter is true
            String dataString = null;
            if (includeData) {
              Optional<S3ObjectSummary> dataObject = entry.getValue().stream().filter(object -> object.getKey().endsWith("/data")).findFirst();
              if (dataObject.isPresent()) {
                InputStream dataIs = s3client.getObject(bucket, dataObject.get().getKey()).getObjectContent();
                dataString = new String(dataIs.readAllBytes());
              }
            }

            // Build and return the OfferedResource object
            return new OfferedResource(UUID.fromString(entry.getKey()), metadataObject.get().getLastModified(), metadataObject.get().getLastModified(), resourceMetadata, dataString);
          } catch (IOException e) {
            e.printStackTrace();
            return null;
          }
        })
        // Remove all unsuccessful attempts at parsing
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }

  /*
     Unused methods for this demonstration, all methods below are not actively used in the OfferedResourceServiceImpl class
   */

  @Override
  public List<OfferedResource> findAll(Sort sort) {
    return null;
  }

  @Override
  public Page<OfferedResource> findAll(Pageable pageable) {
    return null;
  }

  @Override
  public List<OfferedResource> findAllById(Iterable<UUID> iterable) {
    return null;
  }

  @Override
  public long count() {
    return 0;
  }

  @Override
  public void delete(OfferedResource offeredResource) {

  }

  @Override
  public void deleteAll(Iterable<? extends OfferedResource> iterable) {

  }

  @Override
  public void deleteAll() {

  }

  @Override
  public <S extends OfferedResource> List<S> saveAll(Iterable<S> iterable) {
    return null;
  }

  @Override
  public boolean existsById(UUID uuid) {
    return false;
  }

  @Override
  public void flush() {

  }

  @Override
  public <S extends OfferedResource> S saveAndFlush(S s) {
    return null;
  }

  @Override
  public void deleteInBatch(Iterable<OfferedResource> iterable) {

  }

  @Override
  public void deleteAllInBatch() {

  }

  @Override
  public OfferedResource getOne(UUID uuid) {
    return null;
  }

  @Override
  public <S extends OfferedResource> Optional<S> findOne(Example<S> example) {
    return Optional.empty();
  }

  @Override
  public <S extends OfferedResource> List<S> findAll(Example<S> example) {
    return null;
  }

  @Override
  public <S extends OfferedResource> List<S> findAll(Example<S> example, Sort sort) {
    return null;
  }

  @Override
  public <S extends OfferedResource> Page<S> findAll(Example<S> example, Pageable pageable) {
    return null;
  }

  @Override
  public <S extends OfferedResource> long count(Example<S> example) {
    return 0;
  }

  @Override
  public <S extends OfferedResource> boolean exists(Example<S> example) {
    return false;
  }
}
