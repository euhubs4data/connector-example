package de.fraunhofer.isst.dataspaceconnector.repositories;

import de.fraunhofer.isst.dataspaceconnector.model.OfferedResource;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Interface to the repository containing the offered resources.
 */
@Repository
@ConditionalOnMissingBean(OfferedResourceRepository.class)
public interface OfferedResourceRepository extends JpaRepository<OfferedResource, UUID> {

}
