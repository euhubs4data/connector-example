package de.fraunhofer.isst.dataspaceconnector;

import de.fraunhofer.isst.ids.framework.daps.DapsTokenProvider;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ConnectorApplicationTests {
	@Autowired
	DapsTokenProvider tokenProvider;
	@Test
	void contextLoads() {
		System.out.println(tokenProvider.getDAT().getTokenValue());
	}
}
