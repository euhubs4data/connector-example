package de.fraunhofer.isst.dataspaceconnector;

import de.fraunhofer.isst.dataspaceconnector.model.OfferedResource;
import de.fraunhofer.isst.dataspaceconnector.model.ResourceMetadata;
import de.fraunhofer.isst.dataspaceconnector.repositories.OfferedResourceRepository;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@SpringBootTest(classes = ConnectorApplication.class)
@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore("Should only be used when aws properties are configured properly")
public class S3RepositoryTest {
  @Autowired
  public OfferedResourceRepository offeredResourceRepository;

  private static ResourceMetadata resourceMetadata = new ResourceMetadata(
      "ExampleResource",
      "ExampleResourceDescription",
      Collections.emptyList(),
      "Example policy",
      null,
      null,
      "0.0.1",
      Collections.emptyMap(),
      null);
  private static OfferedResource offeredResource = new OfferedResource(UUID.randomUUID(), new Date(), new Date(), resourceMetadata, "Test Document");

  @Test
  public void aCleanRepository() {
    offeredResourceRepository.findAll().forEach(offeredResource -> {
      offeredResourceRepository.deleteById(offeredResource.getUuid());
    });
  }

  @Test
  public void bUploadResources() {
    offeredResourceRepository.save(offeredResource);
    for (int i = 0; i < 100; i++) {
      offeredResourceRepository.save(new OfferedResource(UUID.randomUUID(), new Date(), new Date(), resourceMetadata, "Test Document"));
    }
  }

  @Test
  public void cRetrieveAllResources() {
    List<OfferedResource> resources = offeredResourceRepository.findAll();
    Assert.assertEquals(101, resources.size());
  }

}
